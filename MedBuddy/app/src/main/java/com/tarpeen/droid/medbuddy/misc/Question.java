package com.tarpeen.droid.medbuddy.misc;

import java.util.ArrayList;

/**
 * Created by Saad Manzur
 * Project: MedBuddy
 * Date: 11/11/2016
 * Time: 8:28 AM
 * Copyright stays with the creator.
 */

public class Question {
    public String question;
    public ArrayList<String> data;
    public ArrayList<String[]> subData;
    public ArrayList<String> type;

    public Question(String q){
        data = new ArrayList<>();
        type = new ArrayList<>();
        subData = new ArrayList<>();
        question = q;
    }
}
