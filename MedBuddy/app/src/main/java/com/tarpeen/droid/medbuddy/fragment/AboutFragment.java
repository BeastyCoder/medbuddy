package com.tarpeen.droid.medbuddy.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.activity.MainActivity;
import com.tarpeen.droid.medbuddy.misc.Utilities;

/**
 * Created by Saad on 11/1/2016.
 */

public class AboutFragment extends Fragment {
    private final String TAG = "AboutFragment";

    private View rootView;
    private TextView body;
    private TextView url;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.about_fragment,null);
        MainActivity.setTitle("About Us");
        body = (TextView)rootView.findViewById(R.id.about_text);
        body.setTypeface(Utilities.REGULAR);
        body.setMovementMethod(LinkMovementMethod.getInstance());
        body.setLinkTextColor(ContextCompat.getColor(getActivity(),R.color.anotherBlue));

        /*
        url = (TextView)rootView.findViewById(R.id.about_url);
        url.setTypeface(Utilities.BOLD);
        url.setMovementMethod(LinkMovementMethod.getInstance());
        */

        return rootView;
    }
}
