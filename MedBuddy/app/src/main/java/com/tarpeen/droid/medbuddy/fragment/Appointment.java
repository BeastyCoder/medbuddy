package com.tarpeen.droid.medbuddy.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarpeen.droid.medbuddy.activity.MainActivity;
import com.tarpeen.droid.medbuddy.adapter.SuggestionAdapter;
import com.tarpeen.droid.medbuddy.background.RequestMaker;
import com.tarpeen.droid.medbuddy.misc.Info;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.adapter.AppointmentAdapter;
import com.tarpeen.droid.medbuddy.misc.AppointmentDetails;
import com.tarpeen.droid.medbuddy.misc.Utilities;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Saad on 10/31/2016.
 */

public class Appointment extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private final String TAG = "Appointment";

    private ImageButton ibAdd;
    private ListView lvAppointments;
    private View rootView;
    private ListView lSuggest;
    ArrayList<AppointmentDetails> details = new ArrayList<>();
    ArrayList<Info> info = new ArrayList<>();
    private boolean myEdit = false;

    private AppointmentAdapter mAdapter;
    private SuggestionAdapter mAutoAdapter;
    private RequestMaker mRequestMaker;

    private String dName;
    private String dType;
    private String dTime;
    private String dDate;
    private String dSpec;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setupList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_appointment,null);

        MainActivity.setTitle("Appointment");
        ibAdd = (ImageButton)rootView.findViewById(R.id.appointment_add);
        ibAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                View dView = getActivity().getLayoutInflater().inflate(R.layout.appointment_form,
                        null);



                final EditText etDoctor = (EditText) dView.findViewById(R.id.appointment_form_doctor);
                etDoctor.setTypeface(Utilities.REGULAR);
                etDoctor.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(mRequestMaker != null && mRequestMaker.getStatus() !=
                                AsyncTask.Status.FINISHED)
                            mRequestMaker.cancel(true);

                        Log.d(TAG,s.toString());

                        if(s.toString().length() < 2 || myEdit) {
                            info.clear();
                            mAutoAdapter.notifyDataSetChanged();
                            return;
                        }
                        mRequestMaker = new RequestMaker(getActivity(), new RequestMaker.Communicator() {
                            @Override
                            public void getResponse(String s) {
                                try{
                                    info.clear();
                                    JSONArray jsonArray = new JSONArray(s);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        info.add(new Info(jsonArray.getJSONObject(i).getString("name"),
                                                jsonArray.getJSONObject(i).getString("speciality")));
                                    }
                                    if(info.size() > 2){
                                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                                                ActionBar.LayoutParams.MATCH_PARENT, 120);
                                        lSuggest.setLayoutParams(params);
                                    }
                                    mAutoAdapter.notifyDataSetChanged();
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    Log.d(TAG,e.toString());
                                }
                            }
                        }, false);

                        mRequestMaker.execute("http://greensheba.tarpeen.com/api/doctors_list","q",
                                s.toString());
                    }
                });

                dDate = etDoctor.getText().toString();

                final TextView tvSpec = (TextView)dView.findViewById(R.id.appointment_form_speciality);
                tvSpec.setTypeface(Utilities.REGULAR);

                mAutoAdapter = new SuggestionAdapter(getActivity(), R.id.appointment_form_autosuggest
                        , info, etDoctor, tvSpec, new myFault() {
                    @Override
                    public void changeEdit(boolean flag) {
                        Log.d(TAG,"HERE");
                        myEdit = flag;
                    }
                });

                lSuggest = (ListView)dView.findViewById(R.id.appointment_form_autosuggest);
                lSuggest.setAdapter(mAutoAdapter);

                final Dialog d = new AlertDialog.Builder(getActivity())
                        .setView(dView)
                        .setCancelable(true)
                        .show();

                Button bCancel = (Button)dView.findViewById(R.id.appointment_form_cancel);
                bCancel.setTypeface(Utilities.BOLD);
                bCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.cancel();
                    }
                });

                Button bNext = (Button)dView.findViewById(R.id.appointment_form_next);
                bNext.setTypeface(Utilities.BOLD);
                bNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dName = etDoctor.getText().toString();
                        dSpec = tvSpec.getText().toString();
                        Calendar c = Calendar.getInstance();
                        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                                Appointment.this, c.get(Calendar.YEAR),c.get(Calendar.MONTH),
                                c.get(Calendar.DAY_OF_MONTH));
                        datePicker.setAccentColor(ContextCompat.getColor(getActivity(),
                                R.color.customBlue));
                        datePicker.vibrate(false);
                        datePicker.setMinDate(c);
                        datePicker.setOkText("NEXT");
                        datePicker.setCancelText("CANCEL");
                        datePicker.show(getActivity().getFragmentManager(),"DatePickerDialog");
                        d.cancel();
                    }
                });
            }
        });

        mAdapter = new AppointmentAdapter(getActivity(),R.id.appointment_listview,details);
        lvAppointments = (ListView)rootView.findViewById(R.id.appointment_listview);
        lvAppointments.setAdapter(mAdapter);

        return rootView;
    }

    private void setupList(){
        details.add(new AppointmentDetails("Dr. Ahmed","08:40","2016-11-19","R"));
        details.add(new AppointmentDetails("Amanda Waller","08:40","2016-06-01","E"));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar newDate = Calendar.getInstance();
        newDate.set(year,monthOfYear,dayOfMonth);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        dDate = sdf.format(newDate.getTime());

        Calendar c = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(Appointment.this,
                c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE),c.get(Calendar.SECOND),false);

        tpd.setCancelText("CANCEL");
        tpd.setOkText("REQUEST");
        tpd.setAccentColor(ContextCompat.getColor(getActivity(),
                R.color.customBlue));
        tpd.setCancelable(true);
        tpd.vibrate(false);
        tpd.show(getActivity().getFragmentManager(),"timepicker_dialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        Calendar c = Calendar.getInstance();
        Log.d(TAG,hourOfDay+"");
        c.set(Calendar.HOUR_OF_DAY,hourOfDay);
        c.set(Calendar.MINUTE,minute);
        c.set(Calendar.SECOND,second);

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        dTime = sdf.format(c.getTime());

        dType = "P";
        Toast.makeText(getActivity(),"Submitting",Toast.LENGTH_SHORT).show();

        details.add(new AppointmentDetails(dName,dTime,dDate,dType));
        mAdapter.notifyDataSetChanged();
    }

    public interface myFault{
        void changeEdit(boolean flag);
    }
}
