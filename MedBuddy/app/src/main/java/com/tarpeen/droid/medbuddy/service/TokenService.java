package com.tarpeen.droid.medbuddy.service;


import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tarpeen.droid.medbuddy.misc.Utilities;

public class TokenService extends FirebaseInstanceIdService{
    private final String TAG = "TokenService";

    private SharedPreferences prefs;
    private SharedPreferences.Editor prefEdit;

    public TokenService() {
        Log.d(TAG,"Token Service Started");
    }

    @Override
    public void onTokenRefresh() {
        prefs = getSharedPreferences(Utilities.PREF_FILE,MODE_PRIVATE);
        prefEdit = prefs.edit();

        prefEdit.putString(Utilities.FCM_KEY, FirebaseInstanceId.getInstance().getToken());
        prefEdit.commit();
        prefEdit.apply();
    }
}
