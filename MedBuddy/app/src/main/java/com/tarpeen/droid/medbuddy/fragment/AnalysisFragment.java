package com.tarpeen.droid.medbuddy.fragment;


import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.activity.MainActivity;
import com.tarpeen.droid.medbuddy.adapter.AnalysisAdapter;
import com.tarpeen.droid.medbuddy.background.RequestMaker;
import com.tarpeen.droid.medbuddy.misc.Question;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AnalysisFragment extends Fragment {
    private final String TAG = "AnalysisFragment";

    private View rootView;
    private ArrayList<View> viewsToSwitch;

    private ImageButton ibRight;
    private TextView question;
    private int currentView = 0;
    ArrayList<Question> sets = new ArrayList<>();

    public AnalysisFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_analysis, container, false);
        MainActivity.setTitle("Prognosis");
        question = (TextView)rootView.findViewById(R.id.analysis_title);
        question.setTypeface(Utilities.BOLD);

        question.setText("Select one?");
        firstTime();

        return rootView;
    }

    private void firstTime(){
        viewsToSwitch = new ArrayList<>();

        ArrayList<String> params = new ArrayList<>();
        params.add("http://greensheba.tarpeen.com/api/prog_list_all");
        params.add("q");
        params.add("1");

        String[] arr = params.toArray(new String[0]);

        new RequestMaker(getActivity(), new RequestMaker.Communicator() {
            @Override
            public void getResponse(String s) {
                //Log.d(TAG,s);
                try {
                    JSONArray jArray = new JSONArray(s);

                    for(int i = 0; i < jArray.length(); i++){
                        JSONObject jObj = jArray.getJSONObject(i);
                        Question q = new Question(jObj.getString("question"));
                        JSONArray jArray2 = jObj.getJSONArray("data");
                        for(int j = 0; j < jArray2.length(); j++){
                            q.data.add(jArray2.getJSONObject(j).getString("name"));
                            Log.d(TAG,q.data.get(j));
                            q.type.add(jArray2.getJSONObject(j).getString("select"));
                            JSONArray sub = jArray2.getJSONObject(j).getJSONArray("data_structure");
                            Log.d(TAG,sub.length() + "");
                            String[] temp = new String[sub.length()];
                            q.subData.add(temp);
                            Log.d(TAG,"dwdw");
                            for(int k = 0; k < sub.length(); k++) {

                                q.subData.get(q.subData.size() - 1)[k] = sub.getString(k);
                                Log.d(TAG,q.subData.get(j)[k]);
                            }

                        }
                        sets.add(q);
                    }
                    Log.d(TAG,sets.size()+"");
                    for(int i = 0; i < sets.size(); i++){
                        ListView lv = new ListView(getActivity());
                        lv.setId(View.generateViewId());
                        lv.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        lv.setDividerHeight(0);
                        lv.setDivider(null);
                        lv.setAdapter(new AnalysisAdapter(getActivity(),
                                lv.getId(),sets.get(i).data,sets.get(i).subData,sets.get(i).type));
                        viewsToSwitch.add(lv);
                    }

                    final RelativeLayout relativeLayout = (RelativeLayout)rootView.findViewById(R.id.analysis_switcher);
                    relativeLayout.addView(viewsToSwitch.get(0));
                    question.setText(sets.get(0).question);
                    ibRight = (ImageButton)rootView.findViewById(R.id.analysis_next);
                    ibRight.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(currentView < viewsToSwitch.size()-1) {
                                if(relativeLayout.getChildAt(0) != null)
                                    relativeLayout.removeAllViews();

                                currentView++;
                                question.setText(sets.get(currentView).question);
                                relativeLayout.addView(viewsToSwitch.get(currentView));
                            }
                            else{
                                relativeLayout.removeAllViews();
                                ibRight.setVisibility(View.GONE);
                                TextView tv = new TextView(getActivity());
                                tv.setTypeface(Utilities.BOLD);
                                tv.setPadding(20,20,20,20);
                                tv.setGravity(Gravity.CENTER);
                                question.setText("Our Suggestion");
                                tv.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.anotherBlue));
                                StringBuilder middle = new StringBuilder();
                                AnalysisAdapter aa = (AnalysisAdapter)(((ListView)viewsToSwitch.get(0)).getAdapter());
                                if(aa.selected[0] || aa.selected[1] || aa.selected[2])
                                    middle.append("Consider avoiding fat and cholesterol. ");
                                if(aa.selected[3] || aa.selected[10] || aa.selected[11])
                                    middle.append("Avoid junk, oily food and be more keen on exercise. ");
                                if(aa.selected[4] || aa.selected[8] || aa.selected[9])
                                    middle.append("As breathing problem persists, stay away from cold food, " +
                                            "drinks and dust. ");
                                if(aa.selected[6] || aa.selected[12] || aa.selected[13] || aa.selected[14]
                                    || aa.selected[15] || aa.selected[16])
                                    middle.append("Try not to worry too much, avoid fights and debates. ");
                                tv.setText("According to the data you given your sugar level is marginal." +
                                        middle.toString() +
                                        "Our accelerometer analysis shows that you are not active " +
                                        "enough. You should go out more often and exercise once in a while.");
                                tv.setTextSize(18);
                                tv.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                                relativeLayout.addView(tv);
                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG,e.toString());
                }

            }
        }, false).execute(arr);
    }

}
