package com.tarpeen.droid.medbuddy.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.misc.AppointmentDetails;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Saad on 10/31/2016.
 */

public class AppointmentAdapter extends ArrayAdapter {
    private final String TAG = "AppointmentAdapter";

    private Context mContext;
    private ArrayList<AppointmentDetails> mItems = new ArrayList<>();

    public AppointmentAdapter(Context context, int resource, ArrayList<AppointmentDetails> details){
        super(context, resource);
        mContext = context;
        mItems = details;
    }

    private int getColorId(String t){
        int result = R.color.customBlue;
        if(t.equals("P")) {
            result = R.color.sandStorm;
        }
        else if(t.equals("V")) {
            result = R.color.ecstasy;
        }
        else if(t.equals("O")){
            result = R.color.seance;
        }
        else if(t.equals("E")){
            result = R.color.thunderBird;
        }
        return result;
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.appointment_item,null);
        }

        ImageButton imageButton = (ImageButton)convertView.findViewById(R.id.appointment_item_cancel);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dView = ((Activity)mContext).getLayoutInflater().inflate(
                        R.layout.appointment_cancel,null);

                TextView tvTitle = (TextView)dView.findViewById(R.id.appointment_cancel_title);
                tvTitle.setTypeface(Utilities.REGULAR);
                final Dialog d = new AlertDialog.Builder(mContext)
                        .setView(dView)
                        .setCancelable(true)
                        .show();

                Button bCancel = (Button)dView.findViewById(R.id.appointment_cancel_cancel);
                bCancel.setTypeface(Utilities.BOLD);
                bCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.cancel();
                    }
                });

                Button bOk = (Button)dView.findViewById(R.id.appointment_cancel_next);
                bOk.setTypeface(Utilities.BOLD);
                bOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItems.remove(position);
                        Toast.makeText(mContext,"Appointment Cancelled",Toast.LENGTH_SHORT).show();
                        d.cancel();
                        notifyDataSetChanged();
                    }
                });
            }
        });


        TextView tvType = (TextView)convertView.findViewById(R.id.appointment_item_icon);
        tvType.setTypeface(Utilities.MEDIUM);
        tvType.setBackgroundColor(ContextCompat.getColor(mContext,getColorId(
                mItems.get(position).type
        )));
        tvType.setText(mItems.get(position).type);

        TextView tvTitle = (TextView)convertView.findViewById(R.id.appointment_item_name);
        tvTitle.setTypeface(Utilities.REGULAR);
        tvTitle.setText(mItems.get(position).doctorName);

        TextView tvTime = (TextView)convertView.findViewById(R.id.appointment_item_time);
        tvTime.setTypeface(Utilities.REGULAR);
        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd hh:mm a").parse(mItems.get(position).date + " "
                    + mItems.get(position).time);
            tvTime.setText(new SimpleDateFormat("dd MMM,yyyy hh:mm a").format(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
