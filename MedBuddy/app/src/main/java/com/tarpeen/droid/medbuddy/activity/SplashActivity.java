package com.tarpeen.droid.medbuddy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.tarpeen.droid.medbuddy.R;


/**
 * Created by Saad on 11/1/2016.
 */

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(!checkPlayServices())
            return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this,MainActivity.class));
                SplashActivity.this.finish();
            }
        },SPLASH_TIME);
    }

    private boolean checkPlayServices(){
        GoogleApiAvailability gAPI = GoogleApiAvailability.getInstance();
        int result = gAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS){
            if(gAPI.isUserResolvableError(result)){
                gAPI.getErrorDialog(this,result,9000).show();
            }
            return false;
        }
        else
            return true;
    }

}