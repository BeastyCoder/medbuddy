package com.tarpeen.droid.medbuddy.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tarpeen.droid.medbuddy.activity.MainActivity;
import com.tarpeen.droid.medbuddy.adapter.MedicineAdapter;
import com.tarpeen.droid.medbuddy.adapter.SuggestionAdapter;
import com.tarpeen.droid.medbuddy.background.RequestMaker;
import com.tarpeen.droid.medbuddy.misc.Info;
import com.tarpeen.droid.medbuddy.misc.MedicineDetails;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.adapter.AppointmentAdapter;
import com.tarpeen.droid.medbuddy.misc.AppointmentDetails;
import com.tarpeen.droid.medbuddy.misc.Utilities;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Saad on 11/1/2016.
 */

public class Medicine extends Fragment {
    private final String TAG = "Medicine";

    private View rootView;
    private ListView listView;
    private ListView lSuggest;
    private ImageButton ibAdd;
    private MedicineAdapter medicineAdapter;
    private RequestMaker mRequestMaker;
    private boolean myEdit = false;
    private SuggestionAdapter mAutoAdapter;
    private ArrayList<Info> info = new ArrayList<>();
    private ArrayList<MedicineDetails> details = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_medicine, null);

        MainActivity.setTitle("Medicine");
        listView =(ListView) rootView.findViewById(R.id.medicine_listview);

        medicineAdapter = new MedicineAdapter(getActivity(),R.id.medicine_listview,details);
        listView.setAdapter(medicineAdapter);


        ibAdd = (ImageButton)rootView.findViewById(R.id.medicine_add);
        ibAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View dView = getActivity().getLayoutInflater().inflate(R.layout.medicine_form,null);


                final EditText med = (EditText)dView.findViewById(R.id.medicine_form_name);
                med.setTypeface(Utilities.REGULAR);
                med.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(mRequestMaker != null && mRequestMaker.getStatus() !=
                                AsyncTask.Status.FINISHED)
                            mRequestMaker.cancel(true);

                        Log.d(TAG,s.toString());

                        if(s.toString().length() < 2 || myEdit) {
                            info.clear();
                            mAutoAdapter.notifyDataSetChanged();
                            return;
                        }
                        mRequestMaker = new RequestMaker(getActivity(), new RequestMaker.Communicator() {
                            @Override
                            public void getResponse(String s) {
                                try{
                                    info.clear();
                                    Log.d(TAG,s);
                                    JSONObject jObj = new JSONObject(s);
                                    JSONArray jArray = jObj.getJSONArray("data");
                                    for(int i = 0; i < jArray.length(); i++){
                                        info.add(new Info(jArray.getJSONObject(i).getString("name"),
                                                jArray.getJSONObject(i).getString("generic")));
                                    }
                                    if(info.size() > 2){
                                        lSuggest.setLayoutParams(new ViewGroup.LayoutParams(
                                                ViewGroup.LayoutParams.MATCH_PARENT,
                                                120));
                                    }
                                    mAutoAdapter.notifyDataSetChanged();
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    Log.d(TAG,e.toString());
                                }
                            }
                        }, false);

                        mRequestMaker.execute("http://greensheba.tarpeen.com/api/search","search",
                                s.toString());
                    }
                });

                final TextView tvGroup = (TextView)dView.findViewById(R.id.medicine_form_group);
                tvGroup.setTypeface(Utilities.REGULAR);

                mAutoAdapter = new SuggestionAdapter(getActivity(), R.id.medicine_form_autosuggest
                        , info, med, tvGroup, new Appointment.myFault() {
                    @Override
                    public void changeEdit(boolean flag) {
                        Log.d(TAG,"HERE");
                        myEdit = flag;
                    }
                });

                lSuggest = (ListView)dView.findViewById(R.id.medicine_form_autosuggest);
                lSuggest.setAdapter(mAutoAdapter);

                final EditText dose = (EditText)dView.findViewById(R.id.medicine_form_dosage);
                dose.setTypeface(Utilities.REGULAR);

                final TextView time = (TextView)dView.findViewById(R.id.medicine_form_time);
                time.setTypeface(Utilities.REGULAR);

                time.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar c = Calendar.getInstance();
                        TimePickerDialog tDialog = TimePickerDialog.newInstance(
                                new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute,
                                                  int second) {
                                Calendar newTime = Calendar.getInstance();
                                newTime.set(Calendar.HOUR_OF_DAY,hourOfDay);
                                newTime.set(Calendar.MINUTE,minute);

                                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                                time.setText(sdf.format(newTime.getTime()));
                            }
                        },c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE),false);

                        tDialog.vibrate(false);
                        tDialog.setAccentColor(ContextCompat.getColor(getActivity(),
                                R.color.customBlue));
                        tDialog.setTitle("Starting From");
                        tDialog.setOkText("Done");
                        tDialog.setCancelText("Cancel");
                        tDialog.show(getActivity().getFragmentManager(),"Timepicker");
                    }
                });

                final Dialog d = new AlertDialog.Builder(getActivity())
                        .setCancelable(true)
                        .setView(dView)
                        .show();

                Button bCancel = (Button)dView.findViewById(R.id.medicine_form_cancel);
                bCancel.setTypeface(Utilities.BOLD);
                bCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.cancel();
                    }
                });

                Button bAdd = (Button)dView.findViewById(R.id.medicine_form_next);
                bAdd.setTypeface(Utilities.BOLD);
                bAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm ");
                        try {
                            Date d  = sdf.parse(time.getText().toString());
                            Calendar c = Calendar.getInstance();
                            c.setTime(d);
                            details.add(new MedicineDetails(med.getText().toString(),"I",c,
                                    Integer.parseInt(dose.getText().toString())));
                            medicineAdapter.notifyDataSetChanged();
                            Toast.makeText(getActivity(),"Reminder added", Toast.LENGTH_SHORT).show();

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        d.cancel();
                    }
                });
            }
        });

        demo();
        return rootView;
    }

    private void demo(){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,3);
        c.set(Calendar.MINUTE,30);

        details.add(new MedicineDetails("Paracetamol","O",c,1));
        details.add(new MedicineDetails("Neotack","I",c,12));
        medicineAdapter.notifyDataSetChanged();
    }
}
