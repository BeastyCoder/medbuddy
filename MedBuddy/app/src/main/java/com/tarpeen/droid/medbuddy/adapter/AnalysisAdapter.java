package com.tarpeen.droid.medbuddy.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Saad Manzur
 * Project: MedBuddy
 * Date: 11/11/2016
 * Time: 6:43 AM
 * Copyright stays with the creator.
 */

public class AnalysisAdapter extends ArrayAdapter{
    private final String TAG = "AnalysisAdapter";

    private ArrayList<String> mLabels = new ArrayList<>();
    private ArrayList<String[]> mSub = new ArrayList<>();
    private Context mContext;
    private ArrayList<String> types;

    public boolean[] selected;

    public AnalysisAdapter(Context context, int resource, ArrayList<String> list,
                           ArrayList<String[]> subList, ArrayList<String> type) {
        super(context, resource);
        mLabels = list;
        mContext = context;
        mSub = subList;
        types = type;
        selected = new boolean[list.size()];
        for(int i = 0; i < list.size(); i++)
            selected[i] = false;
        Log.d(TAG,list.size() + "");
    }


    @Override
    public int getCount() {
        return mLabels.size();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.symptoms_item,parent,
                    false);
        }

        TextView tvLabel = (TextView)convertView.findViewById(R.id.symptoms_info);
        tvLabel.setText(mLabels.get(position));
        tvLabel.setTypeface(Utilities.REGULAR);

        ImageView ivIcon = (ImageView)convertView.findViewById(R.id.symptoms_image);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected[position] = (selected[position])? false:true;
                if(mSub.get(position).length > 0){
                    final View dView = LayoutInflater.from(mContext).inflate(R.layout.radio_dialog
                            , null);

                    TextView tvTitle = (TextView)dView.findViewById(R.id.radio_dialog_title);
                    tvTitle.setTypeface(Utilities.BOLD);

                    RadioGroup rg = (RadioGroup)dView.findViewById(R.id.radio_dialog_rg);
                    for(int i = 0; i < mSub.get(position).length; i++){
                        RadioButton arb = new RadioButton(mContext);
                        arb.setLayoutParams(new RadioGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                80
                        ));
                        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(mContext,null);
                        params.setMargins(0,10,0,10);
                        arb.setLayoutParams(params);
                        arb.setPadding(10,10,10,10);
                        arb.setButtonDrawable(ContextCompat.getDrawable(mContext,R.drawable.radio_drawable));
                        arb.setCompoundDrawablePadding(20);
                        arb.setText(mSub.get(position)[i]);
                        arb.setTextColor(ContextCompat.getColor(mContext,R.color.blueGrey));
                        arb.setTextSize(16);
                        arb.setTypeface(Utilities.REGULAR);
                        rg.addView(arb);
                    }

                    final Dialog d = new AlertDialog.Builder(mContext)
                            .setView(dView)
                            .setCancelable(false)
                            .show();

                    Button bNext = (Button)dView.findViewById(R.id.radio_dialog_next);
                    bNext.setTypeface(Utilities.BOLD);
                    bNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            d.cancel();
                        }
                    });

                    Button bCancel = (Button)dView.findViewById(R.id.radio_dialog_cancel);
                    bCancel.setTypeface(Utilities.BOLD);
                    bCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selected[position] = false;
                            d.cancel();
                            notifyDataSetChanged();
                        }
                    });
                }
                notifyDataSetChanged();
            }
        });


        if(selected[position]) {
            tvLabel.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.customBlue));
            tvLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            ivIcon.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.white_tick));
        }
        else {
            tvLabel.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.aliceBlue));
            tvLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));
            ivIcon.setImageDrawable(null);
        }

        return convertView;
    }
}
