package com.tarpeen.droid.medbuddy.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.background.HospitalLocator;
import com.tarpeen.droid.medbuddy.fragment.AboutFragment;
import com.tarpeen.droid.medbuddy.fragment.Appointment;
import com.tarpeen.droid.medbuddy.fragment.CheckSymptoms;
import com.tarpeen.droid.medbuddy.fragment.FindHospital;
import com.tarpeen.droid.medbuddy.fragment.Medicine;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.util.ArrayList;

/**
 * Created by Saad on 10/22/2016.
 */

public class SymptomsAdapter extends ArrayAdapter {
    private final String TAG = "SymptomsAdapter";

    private ArrayList<String> mLabels = new ArrayList<>();
    private Context mContext;

    private int selected = -1;

    public SymptomsAdapter(Context context, int resource, ArrayList<String> headers) {
        super(context, resource);
        mLabels = headers;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mLabels.size();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.symptoms_item,parent,
                    false);
        }

        TextView tvLabel = (TextView)convertView.findViewById(R.id.symptoms_info);
        tvLabel.setText(mLabels.get(position));
        tvLabel.setTypeface(Utilities.REGULAR);

        ImageView ivIcon = (ImageView)convertView.findViewById(R.id.symptoms_image);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = position;
                notifyDataSetChanged();
            }
        });


        if(selected == position) {
            tvLabel.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.customBlue));
            tvLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            ivIcon.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.white_tick));
        }
        else {
            tvLabel.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.aliceBlue));
            tvLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));
            ivIcon.setImageDrawable(null);
        }

        return convertView;
    }
}
