package com.tarpeen.droid.medbuddy.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.background.HospitalLocator;
import com.tarpeen.droid.medbuddy.fragment.Appointment;
import com.tarpeen.droid.medbuddy.misc.Info;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.util.ArrayList;

/**
 * Created by Saad on 11/1/2016.
 */

public class SuggestionAdapter extends ArrayAdapter {
    private final String TAG = "SuggestionAdapter";

    private Context mContext;
    private ArrayList<Info> mInfo = new ArrayList<>();
    private EditText editText;
    private TextView textView;
    private Appointment.myFault delegate;

    public SuggestionAdapter(Context context, int resource, ArrayList<Info> i, EditText name,
                             TextView spec, Appointment.myFault m) {
        super(context, resource);
        mContext = context;
        mInfo = i;
        editText = name;
        textView = spec;
        delegate = m;
    }

    @Override
    public int getCount() {
        return mInfo.size();
    }

    @Nullable
    @Override
    public Info getItem(int position) {
        return mInfo.get(position);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.suggest_item,null);
        }

        TextView tvName = (TextView)convertView.findViewById(R.id.suggest_name);
        tvName.setTypeface(Utilities.REGULAR);
        tvName.setText(mInfo.get(position).name);

        TextView tvSpec = (TextView)convertView.findViewById(R.id.suggest_spec);
        tvSpec.setTypeface(Utilities.BOLDITALIC);
        tvSpec.setText(mInfo.get(position).spec);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(mInfo.get(position).spec);
                delegate.changeEdit(true);
                editText.setText(mInfo.get(position).name);
                delegate.changeEdit(false);
                mInfo.clear();
                notifyDataSetChanged();
            }
        });

        return convertView;
    }


}
