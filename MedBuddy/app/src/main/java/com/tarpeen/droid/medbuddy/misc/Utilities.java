package com.tarpeen.droid.medbuddy.misc;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Saad on 10/27/2016.
 */

public class Utilities {
    public static final String SERVER = "http://medbuddy.tarpeen.com/api/android/";
    public static final String FCM_KEY = "fcmToken";
    public static final String LOGIN_KEY = "loginToken";
    private static final String TAG = "Utilities";

    public static String FCM_TOKEN;

    public static Typeface REGULAR;
    public static Typeface BOLD;
    public static Typeface BOLDITALIC;
    public static Typeface ITALIC;
    public static Typeface MEDIUM;

    public static String PREF_FILE = "";
    public static String LOGIN_TOKEN = "";

    public static boolean ONCE;

    public static String generateRequest(String... values){
        if(values.length % 2 == 1)
            return null;

        StringBuilder request = new StringBuilder();
        for(int i = 0; i < values.length; i++){
            try {
                request.append(URLEncoder.encode(values[i++],"UTF-8"));
                request.append("=");
                request.append(URLEncoder.encode(values[i],"UTF-8"));
                request.append("&");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        request.setLength(request.length()-1);
        return request.toString();
    }

    public static boolean isNetworkAvailable(Context c){
        ConnectivityManager connManager = (ConnectivityManager)c
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return (connManager.getActiveNetworkInfo() != null);
    }
}
