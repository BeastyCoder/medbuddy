package com.tarpeen.droid.medbuddy.background;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Saad on 10/27/2016.
 */

public class HospitalLocator extends AsyncTask<String,Void,String> {
    private final String TAG = "HospitalLocator";

    private ProgressBar progressBar = null;

    public interface Communicator{
        void Response(ArrayList<ArrayList<String>> s);
    }

    private Context mContext;
    private Communicator delegate;

    public HospitalLocator(Context ctx, Communicator c){
        mContext = ctx;
        delegate = c;
        progressBar = (ProgressBar) ((Activity)ctx).findViewById(android.R.id.content)
                .findViewById(R.id.progressBar);
    }

    @Override
    protected void onPreExecute() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        String baseURL = Utilities.SERVER + "optimus.php";
        String requestBody = Utilities.generateRequest("lat",params[0],"lon",params[1]);
        try{
            URL url = new URL(baseURL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            Log.d(TAG,url.toString());
            Log.d(TAG,requestBody);

            OutputStream os = connection.getOutputStream();
            os.write(requestBody.getBytes());
            os.close();

            connection.connect();

            int responseCode =connection.getResponseCode();
            BufferedReader bReader = null;

            if(responseCode > 400){
                bReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            }
            else{
                bReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }

            String line;
            StringBuilder result = new StringBuilder();
            while((line = bReader.readLine()) != null && !isCancelled()){
                result.append(line);
            }

            return result.toString();
        }
        catch(Exception e){
            e.printStackTrace();
            Log.e(TAG,e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s == null)
            return;

        ArrayList<ArrayList<String>> parent = new ArrayList<>();
        try{
            JSONArray jArray = new JSONArray(s);
            for(int i = 0; i < jArray.length(); i++){
                JSONObject jObj = jArray.getJSONObject(i);
                ArrayList<String> child = new ArrayList<>();
                child.add(jObj.getString("lat"));
                child.add(jObj.getString("lon"));
                child.add(jObj.getString("name"));
                child.add(jObj.getString("address"));
                child.add(jObj.getString("phone"));
                child.add(jObj.getString("type"));
                parent.add(child);
            }
        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG,e.toString());
        }

        delegate.Response(parent);
        progressBar.setVisibility(View.GONE);

    }
}
