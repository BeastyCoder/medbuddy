package com.tarpeen.droid.medbuddy.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.activity.MainActivity;
import com.tarpeen.droid.medbuddy.background.HospitalLocator;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.util.ArrayList;

/**
 * Created by Saad on 10/22/2016.
 */

public class FindHospital extends SupportMapFragment implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private final String TAG = "FindHospital";
    private final int FINE_LOCATION_ACCESS_CODE = 23;
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location currentBestLocation;
    private Marker userMarker;
    private boolean locationOnce = false;

    public FindHospital() {
        super();
    }

    public static FindHospital newInstance() {
        FindHospital fh = new FindHospital();
        return fh;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if(mGoogleApiClient == null){
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View v = super.onCreateView(layoutInflater, viewGroup, bundle);
        MainActivity.setTitle("Find Hospital");
        getMapAsync(this);
        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG,"mapReady");
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_CODE);
        } else {

        }
    }

    @Override
    public void onPause() {
        mMap.setLocationSource(null);
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mGoogleApiClient.isConnected()){
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_CODE);
            }
            else {

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                        this);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case FINE_LOCATION_ACCESS_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                            this);
                }
                else{
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,47),15));
                }
        }
    }



    @Override
    public boolean onMarkerClick(Marker marker) {
        if(marker.getSnippet().isEmpty())
            return false;

        View dView = getActivity().getLayoutInflater().inflate(R.layout.dialog_layout,null);

        Log.d(TAG,marker.getSnippet());
        final String[] words = marker.getSnippet().split(";");

        TextView title = (TextView)dView.findViewById(R.id.dialog_title);
        title.setText(words[0]);
        title.setTypeface(Utilities.BOLD);

        TextView additional = (TextView)dView.findViewById(R.id.dialog_text);
        additional.setVisibility(View.GONE);


        TableLayout tableLayout = (TableLayout)dView.findViewById(R.id.dialog_info);
        tableLayout.setVisibility(View.VISIBLE);


        String[] fields = new String[] {"Address","Phone","Type"};

        for(int i = 0; i < fields.length; i++){
            TableRow tableRow1 = new TableRow(getActivity());
            tableRow1.setWeightSum(2);

            TextView column11 = new TextView(getActivity());
            column11.setTextSize(14);
            column11.setTextColor(ContextCompat.getColor(getActivity(),R.color.grey));
            column11.setText(fields[i]);
            column11.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    0.8f
            ));
            column11.setPadding(10,0,10,0);
            column11.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
            column11.setTypeface(Utilities.BOLD);


            TextView column12 = new TextView(getActivity());
            column12.setTextSize(14);
            column12.setTextColor(ContextCompat.getColor(getActivity(),R.color.grey));
            column12.setText(words[i + 1]);
            column12.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1.2f
            ));
            column12.setPadding(10,0,10,0);
            column12.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
            column12.setTypeface(Utilities.REGULAR);

            tableRow1.addView(column11);
            tableRow1.addView(column12);

            tableLayout.addView(tableRow1);
        }


        final Dialog dialog = new AlertDialog.Builder(getActivity())
                .setView(dView)
                .setCancelable(true)
                .show();

        Button bCancel = (Button)dView.findViewById(R.id.dialog_negative);
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        bCancel.setText("Cancel");
        bCancel.setTypeface(Utilities.BOLD);

        Button bCall = (Button)dView.findViewById(R.id.dialog_positive);
        bCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(words[2].equals("null"))
                    return;

                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+words[2]));
                startActivity(i);
            }
        });
        bCall.setTypeface(Utilities.BOLD);
        bCall.setText("Call");


        return true;
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_ACCESS_CODE);
        }
        else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,
                    this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(mMap != null){
            currentBestLocation = location;
            if(userMarker != null)
                userMarker.remove();
            final LatLng pos = new LatLng(location.getLatitude(),location.getLongitude());
            if(!locationOnce) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 15));
                locationOnce = true;
                new HospitalLocator(getContext(), new HospitalLocator.Communicator() {
                    @Override
                    public void Response(ArrayList<ArrayList<String>> s) {

                        LatLngBounds.Builder llBuilder = LatLngBounds.builder();
                        llBuilder.include(pos);
                        for(int i = 0; i < s.size(); i++){
                            LatLng hPos = new LatLng(Double.valueOf(s.get(i).get(0)),
                                    Double.valueOf(s.get(i).get(1)));

                            StringBuilder stringBuilder = new StringBuilder();
                            for(int j = 2; j < s.get(i).size(); j++){

                                stringBuilder.append(s.get(i).get(j));
                                stringBuilder.append(";");
                            }
                            stringBuilder.setLength(stringBuilder.length()-1);
                            llBuilder.include(hPos);

                            mMap.addMarker(new MarkerOptions()
                                    .position(hPos)
                                    .draggable(false)
                                    .flat(false)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_marker_small))
                                    .anchor(0.5f,1.0f)
                                    .snippet(stringBuilder.toString()));
                        }

                        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(llBuilder.build(),100));
                    }
                }).execute(String.valueOf(pos.latitude),String.valueOf(pos.longitude));
            }

            userMarker = mMap.addMarker(new MarkerOptions()
                    .position(pos)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bluedot_small))
                    .flat(true)
                    .anchor(0.5f,0.5f)
                    .snippet(""));


        }
    }
}
