package com.tarpeen.droid.medbuddy.misc;

/**
 * Created by Saad on 10/31/2016.
 */

public class AppointmentDetails {
    public String doctorName = "";
    public String time = "";
    public String date = "";
    public String type = "";
    public String speciality = "";
    public String chamber = "";

    public AppointmentDetails(String n, String t, String d, String tp){
        doctorName = n;
        time = t;
        date = d;
        type = tp;
    }
}
