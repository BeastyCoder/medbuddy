package com.tarpeen.droid.medbuddy.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.fragment.AboutFragment;
import com.tarpeen.droid.medbuddy.fragment.AnalysisFragment;
import com.tarpeen.droid.medbuddy.fragment.Appointment;
import com.tarpeen.droid.medbuddy.fragment.CheckSymptoms;
import com.tarpeen.droid.medbuddy.fragment.FindHospital;
import com.tarpeen.droid.medbuddy.fragment.Medicine;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.util.ArrayList;

/**
 * Created by Saad on 10/22/2016.
 */

public class DrawerAdapter extends ArrayAdapter {
    private final String TAG = "DrawerAdapter";

    private ArrayList<String> mLabels = new ArrayList<>();
    private ArrayList<Drawable> mIcons = new ArrayList<>();
    private FragmentManager mFragManager;

    private int selected = 0;

    public DrawerAdapter(Context context, int resource, ArrayList<String> headers,
                         ArrayList<Drawable> icons, FragmentManager mFrag) {
        super(context, resource);
        mLabels = headers;
        mIcons = icons;
        mFragManager = mFrag;
    }

    @Override
    public int getCount() {
        return mLabels.size();
    }

    private Fragment getFragment(int position){
        Fragment frag = null;

        switch (position){
            case 0:
                frag = new CheckSymptoms();
                break;
            case 1:
                frag = FindHospital.newInstance();
                break;
            case 2:
                if(!Utilities.LOGIN_TOKEN.isEmpty())
                    frag = new Appointment();
                else
                    frag = new Medicine();
                break;
            case 3:
                if(!Utilities.LOGIN_TOKEN.isEmpty())
                    frag = new Medicine();
                else
                    frag = new AboutFragment();
                break;
            case 4:
                if(!Utilities.LOGIN_TOKEN.isEmpty())
                    frag = new AnalysisFragment();
                else
                    frag = new AboutFragment();
                break;
            case 5:
                frag = new AboutFragment();
                break;
        }

        return frag;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.drawer_item,parent,
                    false);
        }

        TextView tvLabel = (TextView)convertView.findViewById(R.id.drawer_item_txt);
        tvLabel.setText(mLabels.get(position));
        tvLabel.setTypeface(Utilities.REGULAR);

        ImageView ivIcon = (ImageView)convertView.findViewById(R.id.drawer_item_ic);
        ivIcon.setImageDrawable(mIcons.get(position));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = position;
                notifyDataSetChanged();
                View rootView = ((Activity) getContext()).getWindow().getDecorView()
                        .findViewById(android.R.id.content);
                DrawerLayout dl = (DrawerLayout) rootView.findViewById(R.id.activity_main);
                FrameLayout fl = (FrameLayout) rootView.findViewById(R.id.nav_drawer);
                dl.closeDrawer(fl);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Fragment frag = getFragment(position);
                        mFragManager.beginTransaction()
                                .replace(R.id.fragment_holder,frag)
                                .addToBackStack(null)
                                .commit();
                    }
                }, 1000);

            }
        });


        if(selected == position) {
            tvLabel.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.customBlue));
            tvLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            ivIcon.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.anotherBlue));
        }
        else {
            tvLabel.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.aliceBlue));
            tvLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));
            ivIcon.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.customBlue));
        }

        return convertView;
    }
}
