package com.tarpeen.droid.medbuddy.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.misc.MedicineDetails;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.util.ArrayList;

/**
 * Created by Saad on 10/31/2016.
 */

public class MedicineAdapter extends ArrayAdapter {
    private final String TAG = "MedicineAdapter";

    private Context mContext;
    private ArrayList<MedicineDetails> mItems = new ArrayList<>();

    public MedicineAdapter(Context context, int resource, ArrayList<MedicineDetails> details){
        super(context, resource);
        mContext = context;
        mItems = details;
    }

    private int getColorId(String t){
        int result = R.color.customBlue;
        if(t.equals("O")){
            result = R.color.seance;
        }
        else if(t.equals("E")){
            result = R.color.thunderBird;
        }
        return result;
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.appointment_item,null);
        }

        ImageButton imageButton = (ImageButton)convertView.findViewById(R.id.appointment_item_cancel);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dView = ((Activity)mContext).getLayoutInflater().inflate(
                        R.layout.appointment_cancel,null);

                TextView tvTitle = (TextView)dView.findViewById(R.id.appointment_cancel_title);
                tvTitle.setTypeface(Utilities.REGULAR);
                tvTitle.setText("Are you sure?");

                final Dialog d = new AlertDialog.Builder(mContext)
                        .setView(dView)
                        .setCancelable(true)
                        .show();

                Button bCancel = (Button)dView.findViewById(R.id.appointment_cancel_cancel);
                bCancel.setTypeface(Utilities.BOLD);
                bCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.cancel();
                    }
                });

                Button bOk = (Button)dView.findViewById(R.id.appointment_cancel_next);
                bOk.setTypeface(Utilities.BOLD);
                bOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItems.remove(position);
                        Toast.makeText(mContext,"Reminder Cancelled",Toast.LENGTH_SHORT).show();
                        d.cancel();
                        notifyDataSetChanged();
                    }
                });
            }
        });


        TextView tvType = (TextView)convertView.findViewById(R.id.appointment_item_icon);
        tvType.setTypeface(Utilities.MEDIUM);
        tvType.setBackgroundColor(ContextCompat.getColor(mContext,getColorId(
                mItems.get(position).type
        )));
        tvType.setText(mItems.get(position).type);

        TextView tvTitle = (TextView)convertView.findViewById(R.id.appointment_item_name);
        tvTitle.setTypeface(Utilities.REGULAR);
        tvTitle.setText(mItems.get(position).name);

        return convertView;
    }
}
