package com.tarpeen.droid.medbuddy.background;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Saad on 10/26/2016.
 */

public class RequestMaker extends AsyncTask<String, Void, String> {
    private final String TAG = "RequestMaker";

    private ProgressBar progressBar = null;
    private boolean showProgress = false;

    public interface Communicator{
        void getResponse(String s);
    }

    private Context mContext;
    private Communicator delegate;

    public RequestMaker(Context ctx, Communicator c, boolean flag){
        mContext = ctx;
        delegate = c;
        progressBar = (ProgressBar) ((Activity)ctx).findViewById(android.R.id.content)
                .findViewById(R.id.progressBar);
        showProgress = flag;
    }

    @Override
    protected void onPreExecute() {
        if(showProgress)
            progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        String baseURL = params[0];
        String[] request = new String[params.length-1];
        System.arraycopy(params,1,request,0,params.length-1);
        String requestBody = Utilities.generateRequest(request);
        try{
            URL url = new URL(baseURL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            OutputStream os = connection.getOutputStream();
            os.write(requestBody.getBytes());
            os.close();

            connection.connect();

            int responseCode = connection.getResponseCode();

            BufferedReader bReader = null;
            if(responseCode >= 400){
                bReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            }
            else{
                bReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }

            StringBuilder response = new StringBuilder();
            String line = "";
            while((line = bReader.readLine())  != null){
                response.append(line);
            }

            return response.toString();
        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG,e.toString());
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s == null)
            return;


        Log.d(TAG,s);
        try {
            delegate.getResponse(s);
        }catch(Exception e){
            Log.d(TAG,e.toString());
        }
        if(showProgress)
            progressBar.setVisibility(View.GONE);
    }
}
