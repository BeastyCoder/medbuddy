package com.tarpeen.droid.medbuddy.fragment;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.activity.MainActivity;
import com.tarpeen.droid.medbuddy.adapter.SymptomsAdapter;
import com.tarpeen.droid.medbuddy.misc.Utilities;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Saad on 10/23/2016.
 */

public class CheckSymptoms extends Fragment {
    private final String TAG = "Prognosis";

    private View rootView;
    private ArrayList<String> symptomsList = new ArrayList<>();
    private SymptomsAdapter mSymptomsAdapter;
    private ImageButton ibRight;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        demo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_symptoms,null);
        MainActivity.setTitle("Check Symptoms");

        if(!Utilities.ONCE){
            final View dView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_layout,null);

            TextView tvTitle = (TextView)dView.findViewById(R.id.dialog_title);
            tvTitle.setTypeface(Utilities.BOLD);
            tvTitle.setText("Do you have any of the listed symptoms?");
            tvTitle.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.thunderBird));

            TextView tvAdd = (TextView)dView.findViewById(R.id.dialog_text);
            tvAdd.setVisibility(View.GONE);

            TableLayout list = (TableLayout) dView.findViewById(R.id.dialog_info);
            list.setVisibility(View.VISIBLE);

            String[] lines = getActivity().getString(R.string.emergency_symptoms).split(";");

            for(int i = 0; i < lines.length; i++){
                TableRow tableRow1 = new TableRow(getActivity());
                tableRow1.setWeightSum(2);

                TextView column11 = new TextView(getActivity());
                column11.setTextSize(14);
                column11.setTextColor(ContextCompat.getColor(getActivity(),R.color.grey));
                column11.setText(lines[i]);
                column11.setLayoutParams(new TableRow.LayoutParams(
                        0,
                        TableRow.LayoutParams.WRAP_CONTENT,
                        2.0f
                ));
                column11.setPadding(10,0,10,0);
                column11.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
                column11.setTypeface(Utilities.MEDIUM);
                tableRow1.addView(column11);
                list.addView(tableRow1);
            }

            final Dialog d = new AlertDialog.Builder(getActivity())
                    .setView(dView)
                    .setCancelable(false)
                    .show();

            Button bCancel = (Button)dView.findViewById(R.id.dialog_negative);
            bCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.cancel();
                }
            });
            bCancel.setText("No");
            bCancel.setTypeface(Utilities.BOLD);

            Button bCall = (Button)dView.findViewById(R.id.dialog_positive);
            bCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(),"PLEASE VISIT A NEARBY HOSPITAL AS SOON AS POSSIBLE",
                            Toast.LENGTH_SHORT).show();
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_holder,FindHospital.newInstance())
                            .addToBackStack(null)
                            .commit();
                    d.cancel();
                }
            });
            bCall.setTypeface(Utilities.BOLD);
            bCall.setText("Yes");
            Utilities.ONCE = true;
        }

        ListView listView = (ListView)rootView.findViewById(R.id.symptoms_listview);
        mSymptomsAdapter = new SymptomsAdapter(getActivity(),R.id.symptoms_listview,symptomsList);

        listView.setAdapter(mSymptomsAdapter);

        ibRight = (ImageButton)rootView.findViewById(R.id.symptoms_submit);
        ibRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_holder,FindHospital.newInstance())
                        .commit();
            }
        });

        return rootView;
    }

    private void demo(){
        String[] symptoms = getActivity().getString(R.string.syms).split(",");

        symptomsList = new ArrayList<>(Arrays.asList(symptoms));
    }
}
