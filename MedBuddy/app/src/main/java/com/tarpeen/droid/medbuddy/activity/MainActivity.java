package com.tarpeen.droid.medbuddy.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.adapter.DrawerAdapter;
import com.tarpeen.droid.medbuddy.background.RequestMaker;
import com.tarpeen.droid.medbuddy.fragment.CheckSymptoms;
import com.tarpeen.droid.medbuddy.fragment.FindHospital;
import com.tarpeen.droid.medbuddy.misc.MedicineDetails;
import com.tarpeen.droid.medbuddy.misc.Utilities;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";

    private ListView lvDrawer;
    private ImageButton ibLogin;

    private DrawerAdapter mDrawerAdapter;
    private RequestMaker requestMaker;
    private  static TextView tvActionTitle;
    private ArrayList<String> labels = new ArrayList<>();
    ArrayList<Drawable> icons = new ArrayList<>();

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvActionTitle = (TextView)findViewById(R.id.action_bar_title);
        tvActionTitle.setTypeface(Utilities.BOLD);

        ibLogin = (ImageButton)findViewById(R.id.ibMainLogin);

        FirebaseInstanceId.getInstance().getToken();

        if(!Utilities.isNetworkAvailable(this)){
            View dView = LayoutInflater.from(this).inflate(R.layout.appointment_cancel,null);

            TextView tvTitle = (TextView)dView.findViewById(R.id.appointment_cancel_title);
            tvTitle.setTypeface(Utilities.REGULAR);
            tvTitle.setText("It seems you do not have network connection. For the best user experience" +
                    " we insist on turning network on. Do you want to turn it on?");

            final Dialog d = new AlertDialog.Builder(this)
                    .setView(dView)
                    .setCancelable(false)
                    .show();

            Button bCancel = (Button)dView.findViewById(R.id.appointment_cancel_cancel);
            bCancel.setTypeface(Utilities.BOLD);
            bCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.cancel();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_holder, new CheckSymptoms())
                            .commit();
                }
            });

            Button bOk = (Button)dView.findViewById(R.id.appointment_cancel_next);
            bOk.setTypeface(Utilities.BOLD);
            bOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
        }
        else
            getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_holder, new CheckSymptoms())
                .commit();

        Utilities.BOLD = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Bold.ttf");
        Utilities.REGULAR = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Regular.ttf");
        Utilities.BOLDITALIC = Typeface.createFromAsset(getAssets(),"fonts/Roboto-BoldItalic.ttf");
        Utilities.ITALIC = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Italic.ttf");
        Utilities.MEDIUM = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Medium.ttf");
        Utilities.ONCE = false;
        Utilities.PREF_FILE = getString(R.string.pref_file);

        prefs = getSharedPreferences(Utilities.PREF_FILE,MODE_PRIVATE);
        if(prefs.contains(Utilities.FCM_KEY))
            Utilities.FCM_TOKEN = prefs.getString(Utilities.FCM_KEY,"n/a");
        else{
            prefs.edit().putString(Utilities.FCM_KEY,FirebaseInstanceId.getInstance().getToken()).apply();
            Utilities.FCM_TOKEN = FirebaseInstanceId.getInstance().getToken();
        }

        if(prefs.contains(Utilities.LOGIN_KEY)){
            Log.d(TAG,"Hello");
            Utilities.LOGIN_TOKEN = prefs.getString(Utilities.LOGIN_KEY,"");
            ibLogin.setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.log_out));
        }


        //Log.d(TAG,Utilities.FCM_TOKEN);

        ArrayList<String> params = new ArrayList<>();
        params.add(Utilities.SERVER+"index.php");
        params.add("id");
        params.add("1");
        params.add("token");
        params.add(Utilities.FCM_TOKEN);

        String[] arr = params.toArray(new String[0]);

        new RequestMaker(this, new RequestMaker.Communicator() {
            @Override
            public void getResponse(String s) {

            }
        },true).execute(arr);

        setupDialog();
        setupDrawer();
    }

    private void setupDrawer(){
        labels.add("Check Symptoms");
        labels.add("Find Hospital");
        if(!Utilities.LOGIN_TOKEN.isEmpty())
            labels.add("Appointments");
        labels.add("Medicine");
        if(!Utilities.LOGIN_TOKEN.isEmpty())
            labels.add("Prognosis");
        labels.add("About Us");



        icons.add(ContextCompat.getDrawable(this, R.drawable.symptoms));
        icons.add(ContextCompat.getDrawable(this, R.drawable.hospital));
        if(!Utilities.LOGIN_TOKEN.isEmpty())
            icons.add(ContextCompat.getDrawable(this, R.drawable.appointment));
        icons.add(ContextCompat.getDrawable(this,R.drawable.reminder));
        if(!Utilities.LOGIN_TOKEN.isEmpty())
            icons.add(ContextCompat.getDrawable(this,R.drawable.prognosis));
        icons.add(ContextCompat.getDrawable(this,R.drawable.about_us));


        mDrawerAdapter = new DrawerAdapter(this,R.id.lv_drawer,labels,icons,
                getSupportFragmentManager());

        lvDrawer = (ListView)findViewById(R.id.lv_drawer);
        lvDrawer.setAdapter(mDrawerAdapter);

        final DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.activity_main);
        final FrameLayout frameLayout = (FrameLayout)findViewById(R.id.nav_drawer);
        ImageButton ibDrawer = (ImageButton)findViewById(R.id.ibMainDrawer);
        ibDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(frameLayout);
            }
        });

        //demo();
    }

    public static void setTitle(String title){
        tvActionTitle.setText(title);
    }

    private void demo(){
        ArrayList<String> params = new ArrayList<>();
        params.add(Utilities.SERVER+"sender.php");
        params.add("id");
        params.add("1");
        params.add("token");
        params.add(Utilities.FCM_TOKEN);

        String[] arr = params.toArray(new String[0]);

        new RequestMaker(this, new RequestMaker.Communicator() {
            @Override
            public void getResponse(String s) {

            }
        },true).execute(arr);
    }

    private void setupDialog(){
        Log.d(TAG,Utilities.LOGIN_TOKEN);
        if(Utilities.LOGIN_TOKEN.isEmpty()){
            Log.d(TAG,"Login");

            ibLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final View dView = LayoutInflater.from(MainActivity.this)
                            .inflate(R.layout.medicine_form,null);

                    final EditText mail = (EditText)dView.findViewById(R.id.medicine_form_name);
                    mail.setTypeface(Utilities.REGULAR);
                    mail.setHint("Email");
                    mail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                    (dView.findViewById(R.id.medicine_form_group)).setVisibility(View.GONE);


                    final EditText pass = (EditText)dView.findViewById(R.id.medicine_form_dosage);
                    pass.setTypeface(Utilities.REGULAR);
                    pass.setHint("Password");
                    pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    pass.setTransformationMethod(PasswordTransformationMethod.getInstance());

                    final TextView time = (TextView)dView.findViewById(R.id.medicine_form_time);
                    time.setVisibility(View.GONE);

                    final Dialog d = new android.app.AlertDialog.Builder(MainActivity.this)
                            .setCancelable(true)
                            .setView(dView)
                            .show();

                    Button bCancel = (Button)dView.findViewById(R.id.medicine_form_cancel);
                    bCancel.setTypeface(Utilities.BOLD);
                    bCancel.setText("SIGN UP");
                    bCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://greensheba.tarpeen.com/register"));
                            startActivity(browserIntent);
                            d.cancel();
                        }
                    });

                    Button bAdd = (Button)dView.findViewById(R.id.medicine_form_next);
                    bAdd.setTypeface(Utilities.BOLD);
                    bAdd.setText("SIGN IN");
                    bAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG,"Signing you in");
                            String m = mail.getText().toString();
                            String p = pass.getText().toString();

                            if(!m.isEmpty() && !p.isEmpty()){
                                ArrayList<String> params = new ArrayList<>();
                                params.add("http://greensheba.tarpeen.com/api/login");
                                params.add("email");
                                params.add(m);
                                params.add("password");
                                params.add(p);
                                params.add("token");
                                params.add(Utilities.FCM_TOKEN);

                                String[] arr = params.toArray(new String[0]);
                                requestMaker = new RequestMaker(MainActivity.this,
                                        new RequestMaker.Communicator() {
                                            @Override
                                            public void getResponse(String s) {
                                                try {
                                                    Log.d(TAG,s);
                                                    JSONObject jObj = new JSONObject(s);
                                                    if(jObj.getString("code").equals("200")){
                                                        Log.d(TAG,jObj.getString("token"));
                                                        prefs.edit()
                                                                .putString(Utilities.LOGIN_KEY,
                                                                        jObj.getString("token"))
                                                                .apply();

                                                        Utilities.LOGIN_TOKEN = jObj.getString("token");
                                                        ibLogin.setImageDrawable(
                                                                ContextCompat.getDrawable(
                                                                        MainActivity.this,R.drawable.log_out));
                                                        labels.add(2,"Appointments");
                                                        icons.add(2,ContextCompat.getDrawable(
                                                                MainActivity.this,R.drawable.appointment
                                                        ));

                                                        labels.add(4,"Prognosis");
                                                        icons.add(4,ContextCompat.getDrawable(
                                                                MainActivity.this,R.drawable.prognosis
                                                        ));
                                                        mDrawerAdapter.notifyDataSetChanged();
                                                        setupDialog();
                                                    }
                                                    else
                                                        Toast.makeText(MainActivity.this,
                                                                jObj.getString("message"),
                                                                Toast.LENGTH_SHORT).show();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, false);
                                requestMaker.execute(arr);
                                d.cancel();
                            }
                            else
                                Toast.makeText(MainActivity.this,"All fields required",
                                        Toast.LENGTH_SHORT).show();


                        }
                    });

                }
            });


        }
        else{
            ibLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG,"Logged Out");
                    prefs.edit().clear().apply();
                    Utilities.LOGIN_TOKEN = "";
                    ibLogin.setImageDrawable(ContextCompat.getDrawable(MainActivity.this,
                            R.drawable.log_in2));
                    labels.remove(2);
                    icons.remove(2);
                    labels.remove(3);
                    icons.remove(3);
                    mDrawerAdapter.notifyDataSetChanged();
                    setupDialog();
                }
            });

        }

    }

    @Override
    protected void onPause() {
        if(requestMaker != null && requestMaker.getStatus() != AsyncTask.Status.FINISHED)
            requestMaker.cancel(true);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(requestMaker != null && requestMaker.getStatus() != AsyncTask.Status.FINISHED)
            requestMaker.cancel(true);
        super.onDestroy();
    }
}
