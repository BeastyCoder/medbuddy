package com.tarpeen.droid.medbuddy.misc;

import java.util.Calendar;

/**
 * Created by Saad on 11/1/2016.
 */

public class MedicineDetails {
    public String name;
    public String type;
    public String group;
    public Calendar time;
    public int dosage = 0;

    public MedicineDetails(String n, String t, Calendar time, int dosage){
        name = n;
        type = t;
        this.time = time;
        this.dosage = dosage;
    }
}
