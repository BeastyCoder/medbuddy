package com.tarpeen.droid.medbuddy.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.*;
import com.tarpeen.droid.medbuddy.R;
import com.tarpeen.droid.medbuddy.activity.MainActivity;

public class MessagingService extends FirebaseMessagingService{
    private final String TAG = "MessagingService";

    public MessagingService() {

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG,remoteMessage.getData().get("message"));

        Notify(remoteMessage.getData().get("message"));
    }

    private void Notify(String message){
        NotificationManager notificationManager = (NotificationManager)getSystemService(
                NOTIFICATION_SERVICE);

        Intent i = new Intent(this, MainActivity.class);

        PendingIntent pIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder noteBuilder = new NotificationCompat.Builder(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            noteBuilder.setSmallIcon(R.drawable.beaker);
        } else {
            noteBuilder.setSmallIcon(R.drawable.beaker);
        }

        Notification note = noteBuilder
                .setContentTitle("Message from Med Buddy Cloud")
                .setContentText(message)
                .setColor(ContextCompat.getColor(this, R.color.customBlue))
                .setContentIntent(pIntent)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.YELLOW,3000,3000)
                .setAutoCancel(false)
                .build();

        notificationManager.notify(0,note);
    }

}
